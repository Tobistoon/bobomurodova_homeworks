import java.util.Scanner;
public class HW1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

        int smallestDigit = 9;
        while (a != -1) {
            while (a != 0) {
                int lastDigit = a % 10;
                if (lastDigit < smallestDigit) {
                    smallestDigit = lastDigit;
                }
                a = a / 10;
            }
            a = scanner.nextInt();
        }
        System.out.println("Result -" + smallestDigit);
    }
}
